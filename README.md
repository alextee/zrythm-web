# zrythm-web

Website source for https://www.zrythm.org

# LICENSE
Copyright (C) Alexandros Theodotou licensed under the AGPLv3+ license.

This project includes software from https://github.com/aksakalli/jekyll-doc-theme licensed under the MIT license.
Original copyright notice is found at jekyl-doc-theme.LICENSE
