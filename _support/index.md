---
title: Contributions
permalink: /support/
---

Donations are accepted and welcome. They can be made through:
- [PayPal](https://paypal.me/alextee90) - direct donation to the lead developer
- [LiberaPay](https://liberapay.com/alextee) - preferred platform for recurring donations
- [Patreon](https://www.patreon.com/alextee) - recurring donations platform, not recommended but most popular

Contributions can also be made through translating Zrythm, submitting bug reports and feature requests, editing this website, editing the [manual](https://docs.zrythm.org) or submitting songs to be published here. PRs are also welcome from developers. Visit the [chatrooms](https://gitlab.com/alextee/zrythm#chatrooms) to get involved.
